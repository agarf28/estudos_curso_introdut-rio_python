"""
Faça um programa que itera uma string e toda vez que uma vogal aparecer
na sua string print o seu nome entre as letras.

"""



palavra = "abracadabra"

for letra in palavra:
    if letra == "a":
        print("Ronaldo")
    elif letra == "e":
        print("Ronaldo")
    elif letra == "i":
        print("Ronaldo")
    elif letra == "o":
        print("Ronaldo")
    elif letra == "u":
        print("Ronaldo")
    else:
        print(letra)

palavra1 = "abacate"
vogais  = "aeiou"

for letra in palavra1:
    if letra in vogais:
        print("Eduardo")
    else:
        print(letra)  
