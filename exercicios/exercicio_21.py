"""
Dada uma lista de entradas de usuário de números inteiros,
construa um dicionário com lista padrão, a lista dos valores
elevados ao quadrado e a lista dos valores elevados ao cubo.
"""
def quadrado(lista_de_numeros):
    lista_resposta = [] 
    for numero in lista_de_numeros:
        lista_resposta.append(numero ** 2)

    return lista_resposta

def cubo(lista_de_numeros):
    lista_resposta = [] 
    for numero in lista_de_numeros:
        lista_resposta.append(numero ** 3) 

    return lista_resposta

lista_de_valores = [] 

for valor in range(10):
    lista_de_valores.append(
        int(input("Fala um número aí:  "))
    ) 
    
dicionario = {
    'lista padrão': lista_de_valores,
    'lista quadrada': quadrado(lista_de_valores),
    'lista cúbica': cubo(lista_de_valores)

}

print(dicionario)
