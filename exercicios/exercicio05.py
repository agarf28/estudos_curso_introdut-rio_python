"""
Faça um programa para leitura de duas notas
parciais de um aluno. O programa deve cal-
cular a média alcançada por aluno e
apresentar.
"""



nota_1 = int(input("Coloque a nota 1:  "))
nota_2 = int(input("Coloque a nota_2:  "))

media = (nota_1 + nota_2) / 2

if media == 10:
    print("Aprovado com Distinção")
elif media >= 7 and media != 10:
    print("Aprovado")
else:
    print("Reprovado")
