# def > define, definir
# nome (argumento ou parêntese)
# retorn -> devolver

"""
definir nome (parametro 1, parametro 2)
    devolve alguma_coisa

"""

def diga_ola (nome_da_pessoa):
    return f'Olá {nome_da_pessoa}'

# diga_ola("Eduardo")
# resultado = diga_ola("Eduardo")

print(diga_ola("Eduardo"))
print(diga_ola("Ronaldo"))
print(diga_ola("Zulma"))
print(diga_ola("Valdemar"))
