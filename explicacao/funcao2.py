# SUM = soma
# LEN = TAMANHO


def soma(numero_1, numero_2):
    return numero_1 + numero_2

def média(lista_de_numeros):
    return sum(lista_de_numeros) / len(lista_de_numeros)

def imprime_relatório(nome, cpf, telefone):
    return f"""
    Relatório_parcial

    {nome}, portador do cpf {cpf}, que usa o número {telefone}
    está oficialmente de férias.

    Ass. Direção

    """

print(imprime_relatório(nome='Ronaldo Fraga', cpf=1223456, telefone=32218200))
