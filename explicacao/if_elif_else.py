# Extruturas de decisão
# Estrutura condicional

# IF = SE

# Papai, você comprou pão?
# se sim ou se não
# se comprou, ele vai chorar
# se não comprou, ele vai chorar
# elif - else if - Se não, outra coisa
# else - se não - sem resposta


reposta = input("papai, você comprou pão?  ")

if reposta == "sim":
    print("Obrigado, Papai")
elif reposta == "não":
    print("Estou chorando, buá. buá")
else:
    print("Nao foi o que eu perguntei")
