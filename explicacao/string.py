# string, aspas. aspas duplas e triplas.
# concatenação

texto = """
Oi, meu nome é Juvenal

Eu tenho apenas 12 anos de "programação avançada".
"""

print(texto)


minha_mensagem = "olá Aline, você é muito legal"
outra_mensagem = "você tem quantos anos?"

print(minha_mensagem + ". " + outra_mensagem)

print("*" * 20)
print("****Meu Programa****")
print("*" * 20)
